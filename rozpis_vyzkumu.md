## Knihovna

- [ ] geografické mapy - `600o`
- [ ] báje a pověsti pro děti - `200o`
- [ ] báje a pověsti pro dospělé - `400o`
- [ ] báje a pověsti pro výzkumníky - `400o`
- [ ] překládáme jakýkoliv cizí jazyk sami a snadno - `800o`
- [ ] tučňáčí jazyk pro samouky - `555o`
- [ ] velký slovník tučňáčího jazyka - `777o`
- [ ] velký soubor komedií - `415o`
- [ ] Kniha Dezinformace a jak je odhalit - `657o`
- [ ] polní kuchařka pro naprosté začátečníky - `450o`
- [ ] Kniha Používáme Linux a chmod - `0o111`

## Laboratoř

- [ ] extraktor rostlinných přísad - `1000o`
- [ ] nerozbitné zkumavky pro nešikovné ruce - `900o`
- [ ] orbitální telemetrie - `3500o`
- [ ] elixír odolnosti proti starověkým kletbám - `666o`
- [ ] malá vesmírná komunikační anténa - `1 100o`
- [ ] velká vesmírná komunikační anténa - `1 550o`
- [ ] orbitální satelit pro komunikaci s atypickými lidskými formami (ALF) - `3 000o`
- [ ] protininjovský stropní repelent - `800o`
- [ ] syntetizátor relativního absolutna - `664o`
- [ ] počítač - `500o`

## Denní užití

- [ ] lepší lopaty I - `200o`
- [ ] lepší lopaty II - `400o`
- [ ] lepší mačety I - `250o`
- [ ] lepší mačety II - `450o`
- [ ] nesmazatelné fixy - `30o`
- [ ] velké gumáky - `150o`
- [ ] větší termosky - `100o`
- [ ] kapesní kalkulačka - `60o`
- [ ] vědecká kalkulačka - `300o`
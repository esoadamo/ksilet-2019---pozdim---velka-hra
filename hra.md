# Velká hra

### Příběh

V tučňáčí kultuře existuje pověst o mocném elixíru, který by, alespoň dočasně, propůjčil tučňákům schopnost létat. Pověst, která o tomto elixíru mluví, říká, že složky, ze kterých se dá tento elixír vytvořit, byli zapsány pradávnými tučňáky -mysliteli v bájném městě Tučňatlantida. Až doteď se celá tato pověst považovala za naprostý mýtus, nicméně v posledním týdnu se v tučňáčím hlavním městě začali šířit zvěstí, že někdo našel důkaz o jeho existenci. Toho se okamžitě chytlo tučňáčí ministerstvo pro chutné ryby a věděcký a historický výzkum obecně, které si vyžádalo několik expertních týmů, které dostali jednoduchý úkol - vytvořit tento elixír.

Do začátku dostal každý tým `5 000 oblázků`. Každý ve  vašem týmu má trochu jinak postavené schopnosti. Musíte spolupracovat, abyste došli k elixíru dříve než konkurence, čímž si pojistíte výhru. Každá akce, kterou provedete, bude trvat určitý čas a bude vás stát určité peníze. Vaše schopnosti mají možnost tento čas snížit, zároveň, pokud vás bude určitou věc provádět více z vás zároveň, budou se vaše schopnosti sčítat. Tým nemůže provádět více stejných akcí paralelně. Jediný způsob, jak si můžete peníze doplnit, je zveřejňovat vaše výsledky v měsíčních novinách, čímž si získáte investory. Má to ale háček - čím víc toho o sobě uveřejníte, tím víc se ostatní týmy dozví o vašem postupu a budou jej od vás moci opsat.

Zároveň ale nesmíte věřit všemu, co v novinách čtete. Ostatní týmy totiž mohou zveřejnit lživou informaci, za kterou sice nedostanou peníze, ale mohla by vás přivést na zcestí.

- nebojácnost ovlivňuje akce, při kterých se cestuje do terénu
- kreativita pomáhá při tvoření nových postupů, místností, zařízení
- charisma se hodí pro získávání investorů a přesvědčování ostatních

Dobrým začátkem by bylo asi navázat kontakt s tím, který do hlavního města zprávu o bájném městě donesl.

*Jak hra probíhá: Vždy, když budete chtít provést nějakou akci, přijdete za game masterem, kterému řeknete jakou akci provádíte, dáte mu kartičky lidí, kteří tuto akci provádí. Game master poté provede zápis do záznamu událostí, řekne vám, jak dlouho tato akce potrvá a popřípadě upraví váš inventář. Postupem času budete postupovat v příběhu a budou se vám objevovat nové možnosti.*

### Herní plán

Týmy začínají s `5000 o`

Hra je dimenzována na cca `120 minut` čistého času => každá fáze by měla zabrat `30 minut`.

Náklad je předpokládán na `10 000 oblázků => 2 500 oblůzků / fáze`

Měsíc trvá `8 minut`.

- každý měsíc mohou týmy vydat měsíční statistiku o svém působení za `500 oblázků`

Hned ze začátku dostanou všechny týmy: kartičky postav, inventář, rozpis výzkumů

#### Fáze hledání města

Správné město je `D`.

Fáze pokračuje tak dlouho, dokud tým nenasbírá alespoň 20 bodů

Za prvních

- 5 bodů se tým dozví kontinent a dostane mapu kontinentu - za zveřejnění odměna `300 o`
- 10 - se hledaná oblast zmenší na polovinu
- 15 - kvadrant
- 20 - ví se město přesně

Body jsou za i tyto výzkumy:

- geografické mapy - `3 b`
- báje a pověsti pro děti - `1 b`
- báje a pověsti pro dospělé -  `2 b`
- báje a pověsti pro výzkumníky - `2 b`
- orbitální telemetrie - `10b`

PR:

- zveřejnění správného města: `1004 o`

Mají na výběr akce (`12b`, `1000o`, `30min`):

- `N` - chodit a vyptávat se o městu - `1b, 9m, 100 o`
- `K` - jít do archivů a hledat tam - `2b, 10m, 100 o`
- -`C` - inzeráty do novin - `1b, 7m, 200 o`

Jakmile dojde alespoň k objevení kontinentu, je dostupná akce:

- zkontrolovat, zda-li se jedná o správné město - pokud se jedná o správné město, postupuje tým okamžitě do další fáze. Pokud ne, nic se neděje - `15m, 900 o`

#### Fáze hledání budovy

Správná budova je `Papírnictví`

Znovu, 20 bodů. Týmy dostanou mapu města

Prvních 

- 5 bodů zmenší mapu o čtvrtinu
- 10 polovinu
- 15 na čtvrtinu

PR:

- zveřejnění správné budovy - `700 o`
- zveřejnění správného nápisu - `1100 o`

Výzkum:

- geografické mapy - `2b`
- lepší lopaty I - `1b`
- lepší lopaty II - `2b`

Akce (`30 minut, 17 b, 1 300o`):

- `N` - vykopat budovu a určit, jestli se jedná o správnou - `3b, 8m, 400o`
- `K` - hledání v archivech - `2b, 6.5m, 200o`

Události:

- pokud vejdou do budovy, ve které je kletba a nemají aktivní elixír proti starověkým kletbám, stojí `5` minut
- při návratu se správným nápisem, zasáhne bouřka, která rozmočí nápis všem, kteří neměli vyzkoumané nesmazatelné fixy - vrací se zpět `7 minut`
- pří návratu se správným zápisem, zasáhne bouřka a zpomalí návrat o `3` minuty, pokud nemají vyzkoumané velké gumáky

### Fáze překladu nápisu

Jakmile dokončí tým návrat z města, ozve se v rádiu, které běží celý den na ústředně, prapodivné praskání a nesrozumitelná slova. Okamžitě se rozběhne v novinách zpráva, která říká, že byla zachycena posloupnost čtyř čísel z vesmíru a tato posloupnost se stále opakuje. Vláda prý nechce podávat žádné další komentáře, nicméně se prý některým soukromým subjektům také podařilo tuto zprávu zachytit, takže je možné ji zachytit i pomocí soukromých antén i satelitů.

- 20 bodů
  - 5 bodů - první slovo - `červenka červená`
  - 10 druhé -  `žlutěnka žlutá`
  - 15 třetí - `červenka žlutá`
  - 20 čtvrté - `tuřín z džungle`

Výzkum:

- překládáme jakýkoliv cizí jazyk sami a snadno - zrychlení  brainstorimingu o 10%
- tučňáčí jazyk pro samouky - zrychlení  brainstorimingu o 10%
- velký slovník tučňáčího jazyka - zrychlení  brainstorimingu o 10%
- malá / velká vesmírná komunikační anténa / ALF komunikace - dovolí zachycení zprávy (zpráva je správný poměr ukázaný v následující fázi) - pokud má tým toto vyzkoumáno, je jim správný poměr okamžitě prozrazen

PR může zveřejnovat přísady, každou za `800 oblázků`

PR může zveřejnit správný poměr, za `2500 o` 

Nápis má 4 slova, jsou to přísady použité k výrobě elixíru.

Akce (`30 m, 20 b, 1000 o`):

- `N` - výpravy za znalci jazyka žijícími v odlehlých Ledových horách - `2b, 400o, 13 minut`
- `K` - intenzivní brainstorming za použití slovníků, tučňáků a interpolace  - `3b, 200 o, 10 minut`
- `P` - najít člověka, který zachytil zprávu a podplatit ho, aby jej vydal - `2b, 200 o, 6.5 minut`
- `P` - podplatit úředníka, který vyzradí obsah zprávy - `0b, 1000 o, 7 m` - tučňáčtí úředníci jsou neúplatní. Zaplať pokutu `600o`

Události:

- Jakmile se týmu podaří přeložit nápis, vyšle na ně kapitalistická tučňáčí letecká společnosti nájemné ninjy, kteří přeložený nápis zničí a všechna data. Týmy musí podstoupit akci `K, 15min, 500o`, aby obnovily všechna ztracená data.

### Fáze výprav do džungle a syntetizace

V této fázi vyráží náš neohrožený tým do džungle, kde získává přísady. Vždy může vzít maximálně 7 kusů přísad, které se pak zapíší do inventáře. Zároveň se může také může tým pokusit syntetizovat výsledný elixír.

Správný poměr: `čč : žž : čž : td = 1 : 2 : 1 : 3 `

Akce :

- `N` - jít do džungle pro přísady - musí se říct jaké, max 7 - `500o, 15minut`
- `K` - syntetizovat elixír - musí se říct poměr, pokud je správný, tým vyhrává, pokud je špatný, tým přijde o použité přísady a musí pro ně jít znovu - `770o, 14minut`

Výzkum:

- extraktor rostliných přísad - zrychlí syntetizaci o 20%
- nerozbitné zkumavky pro nešikovné ruce - zrychlí syntetizaci o 15%
- lepší mačety I - zrychlí postup džunglí o 10%
- lepší mačety II - zrychlí postup džunglí o 20%